<?php

/**
 * Implements hook_block_view_alter()
 */
function wot_teammate_theme_block_view_alter (&$data, $block) {
  
  if ($block->bid == 'wot_api-login') {
    $data['subject'] = '';
    if (empty($data['content']['login']['#attributes']['class'])) {
      $data['content']['login']['#attributes']['class'] = array();
    }
    $data['content']['login']['#attributes']['class'][] = 'btn-success';
    $data['content']['login']['#attributes']['class'][] = 'btn-large';
  }
}

/**
 * Implements hook_preprocess_page()
 */
function wot_teammate_theme_preprocess_page(&$vars) {
  if ($vars['is_front']) {
    if (empty($vars['title'])) {
      $vars['title'] = variable_get('site_name', "Default site name");
    }
    
    // This will remove the 'No front page content has been created yet.'
    $vars['page']['content']['system_main']['default_message'] = array(); 
  }
}