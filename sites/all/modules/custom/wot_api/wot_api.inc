<?php

/**
 * @file contains Wargamin API & Drupal bridges related code.  
 */

/**
 * Request url's storage.
 */
function wot_api_request_url($type = '') {
  $urls = array(
    'login' => 'https://api.worldoftanks.ru/wot/auth/login/',
    'user_tanks' => 'http://api.worldoftanks.ru/wot/account/tanks/',
    'all_tanks' => 'http://api.worldoftanks.ru/wot/encyclopedia/tanks/',
    'account_statistic' => 'http://api.worldoftanks.ru/wot/account/info/',
  );
  
  // Some aliases
  $urls['auth'] = $urls['login'];
  
  if (!empty($type)) {
    if(array_key_exists($type, $urls)) {
      return $urls[$type];
    } 
    else {
      throw new Exception('Have no API url for given a type.');
    }
  }
  
  return $urls;
}

/**
 * Return wargaming application id.
 */
function wot_api_app_id () {
  $id = variable_get('wot_api_application_id', '');
  if (empty($id)) {
    throw new Exception('wot_api_application_id is not set');
  }
  return $id;
}

function wot_api_settings() {
  $settings = array(
    'app_id' => wot_api_app_id(),
    'lang' => variable_get('wot_api_language', 'en'),
    'log_requests_responses' => variable_get('wot_api_log_requests_responses', FALSE),
  );
  return $settings;
}

/**
 * Return access_token & other informaion if user autorized with Wargaming.Net ID (OpenID).
 */
function wot_api_user () {
  if (!empty($_SESSION['wot_api']) && $_SESSION['wot_api']['expires_at'] > REQUEST_TIME) {
    return $_SESSION['wot_api'];
  }
  return false;  
}

/**
 * Write Wargaming.Net autorization data to Drupal.
 */
function wot_api_authorise_user($data) {
  /*
   * For now just save user data in a session
   * ToDo: attach data to drupal users & assign drupal roles
   */
  $_SESSION['wot_api'] = array(
    'access_token' => $data['access_token'],
    'nickname' => $data['nickname'],
    'account_id' => $data['account_id'],
    'expires_at' => $data['expires_at'],
  );
  module_invoke_all('wot_api_authorise_user', $data);
}

/**
 * Return logged user tanks.
 * Format: 
 * array();
 */
function wot_api_load_user_tanks($fields = array()) {
  
  $user_data = wot_api_user();
  if (empty($user_data)) {
    throw new Exception('User data is empty');
  }
  
  $cid = __FUNCTION__.'_'.$user_data['account_id'];
  $cache = cache_get($cid);
  if (!empty($cache->data)) {
    return $cache->data;
  }
  
  $api_settings = wot_api_settings();
  
  if (is_array($fields) && !empty($fields)) {
    $fields_to_return = $fields;
  }
  elseif ($fields == 'all') {
    $fields_to_return = array();
  } 
  else {
    $fields_to_return = array(
      'in_garage',
      'mark_of_mastery',
      'tank_id',
      'statistics.battles',
      'statistics.wins',
      'statistics.win_and_survived',
    );
  }

  
  $query_data = array(
    'application_id' => $api_settings['app_id'],
    'language' => $api_settings['lang'],
    'account_id' => $user_data['account_id'],
    'access_token' => $user_data['access_token']
  );
  if (!empty($fields) && is_array($fiels)) {
    $query_data['fields'] = implode(',', $fields_to_return);
  }
  
  $full_url = url(wot_api_request_url('user_tanks'), array('query' => $query_data));
  $response = drupal_http_request($full_url);
  $status = (isset($response->status_message)) ? strtolower($response->status_message) : false;
  if ($api_settings['log_requests_responses']) {
    watchdog(__FUNCTION__, 'Request: @request', array('@request' => $full_url), 
      WATCHDOG_INFO);
    watchdog(__FUNCTION__, 'Responce, the responce: @responce', array('@responce' => var_export($response, true)), 
      WATCHDOG_INFO);
  }
  
  if ($status == 'ok') {
    $data = json_decode($response->data, TRUE);
    if ($data['status'] == 'ok') {
      $user_tanks = $data['data'][$user_data['account_id']];
      cache_set($cid, $user_tanks);
      return $user_tanks;
    }
  }
  
  // Report request error
  watchdog('error', 'Bad API server responce, the request: @request', array('@request' => $full_url), 
    WATCHDOG_ERROR);
  
  watchdog('error', 'Bad API server responce, the responce: @responce', array('@responce' => var_export($response, true)), 
    WATCHDOG_ERROR);
      
  return array();
}

function wot_api_load_all_tanks($fields = '') {
  $cid = __FUNCTION__;

  $cache = cache_get($cid);
  if (!empty($cache->data)) {
    return $cache->data;
  }
  
  if (is_array($fields) && !empty($fields)) {
    $fields_to_return = $fields;
  }
  else {
    $fields_to_return = array();
  } 

  $api_settings = wot_api_settings();
  $query_data = array(
    'application_id' => $api_settings['app_id'],
    'language' => $api_settings['lang'],
  );
  if (!empty($fields) && is_array($fiels)) {
    $query_data['fields'] = implode(',', $fields_to_return);
  }
  
  $full_url = url(wot_api_request_url('all_tanks'), array('query' => $query_data));  
  $response = drupal_http_request($full_url);
  
  if ($api_settings['log_requests_responses']) {
    watchdog(__FUNCTION__, 'Request: @request', array('@request' => $full_url), 
      WATCHDOG_INFO);
    watchdog(__FUNCTION__, 'Responce, the responce: @responce', array('@responce' => var_export($response, true)), 
      WATCHDOG_INFO);
  }
  
  $status = (isset($response->status_message)) ? strtolower($response->status_message) : false;
  if ($status == 'ok') {
    $data = json_decode($response->data, TRUE);
    
    if ($data['status'] == 'ok') {
      cache_set($cid, $data['data']);
      return $data['data'];
    }
  }
  
  // Report request error
  watchdog('error', 'Bad API server responce, the request: @request', array('@request' => $full_url), 
    WATCHDOG_ERROR);
  
  watchdog('error', 'Bad API server responce, the responce: @responce', array('@responce' => var_export($response, true)), 
    WATCHDOG_ERROR);
      
  return array();
}

/**
 * Return nations list related to logged in (to wargaming) user.
 */
function wot_api_load_user_tank_nations_list() {
  $user_data = wot_api_user();
  $cid = __FUNCTION__.'_'.$user_data['account_id'];
  $cache = cache_get($cid);
  if (!empty($cache->data)) {
    return $cache->data;
  }
  
  $all_tanks = wot_api_load_all_tanks();
  $user_tanks = wot_api_load_user_tanks();
  $user_data = wot_api_user();
  
  $nations = array();
  foreach ($user_tanks as $user_tank) {
    if (isset($all_tanks[  $user_tank['tank_id']  ])) {
      if (!array_key_exists($all_tanks[  $user_tank['tank_id']  ]['nation'], $nations)) {
        $nations[  $all_tanks[ $user_tank['tank_id'] ]['nation']  ] = $all_tanks[ $user_tank['tank_id'] ]['nation_i18n'];
      }
    }
  }
  cache_set($cid, $nations);
  return $nations;
}



/**
 * Return nations list related to logged in (to wargaming) user.
 * Can be specific to nation.
 */
function wot_api_load_user_tank_tiers_list($nation = '') {
  $user_data = wot_api_user();
  $cid = __FUNCTION__.'_'.$user_data['account_id'].'_'.$nation;
  $cache = cache_get($cid);
  if (!empty($cache->data)) {
    return $cache->data;
  }
  
  $all_tanks = wot_api_load_all_tanks();
  if (empty($all_tanks)) {
    throw new Exception("Can't load tanks list");
  }
  
  $user_tanks = wot_api_load_user_tanks();
  if (empty($user_tanks)) {
    throw new Exception("Can't load user tanks list");
  }
  
  $tiers = array();
  if (empty($nation)) {
    foreach ($user_tanks as $user_tank) {
      if (isset($all_tanks[ $user_tank['tank_id'] ])) {
        if (!array_key_exists($all_tanks[ $user_tank['tank_id'] ]['level'], $tiers)) {
          $tiers[  $all_tanks[ $user_tank['tank_id'] ]['level']  ] = $all_tanks[ $user_tank['tank_id'] ]['level'];
        }
      }
    }
  } else {
    foreach ($user_tanks as $user_tank) {
      if (isset($all_tanks[ $user_tank['tank_id'] ])) {
        if (!array_key_exists($all_tanks[ $user_tank['tank_id'] ]['level'], $tiers) 
        && $all_tanks[ $user_tank['tank_id'] ]['nation'] == $nation) {
          $tiers[  $all_tanks[ $user_tank['tank_id'] ]['level']  ] = $all_tanks[ $user_tank['tank_id'] ]['level'];
        }
      }
    }
  }

  cache_set($cid, $tiers);
  return $tiers;
}

/**
 * Calculate from all tanks list
 */
function wot_api_load_user_tank_models_list($nation = '', $tier = '') {
  $user_data = wot_api_user();
  $cid = __FUNCTION__.'_'.$user_data['account_id'].'_'.$nation.'_'.$tier;
  $cache = cache_get($cid);
  if (!empty($cache->data)) {
    return $cache->data;
  }
  
  $all_tanks = wot_api_load_all_tanks();
  $user_tanks = wot_api_load_user_tanks();
  
  $models = array();

  foreach ($user_tanks as $user_tank) {
    if (isset($all_tanks[ $user_tank['tank_id'] ])) {
      $filters_ok = TRUE;
      if (!empty($nation) && $all_tanks[ $user_tank['tank_id'] ]['nation'] != $nation) {
        $filters_ok = FALSE;
      }
      if (!empty($tier) && $all_tanks[ $user_tank['tank_id'] ]['level'] != $tier) {
        $filters_ok = FALSE;
      }
         
      if ($filters_ok &&  !array_key_exists($all_tanks[ $user_tank['tank_id'] ]['tank_id'], $models)) {
        $models[ $all_tanks[ $user_tank['tank_id'] ]['tank_id'] ] = $all_tanks[ $user_tank['tank_id'] ]['name_i18n'];
      }
    }
  }

  cache_set($cid, $models);
  return $models;
}

function wot_api_load_user_statistic() {
  $user_data = wot_api_user();
  $cid = __FUNCTION__.'_'.$user_data['account_id'];
  $cache = cache_get($cid);
  if (!empty($cache->data)) {
    return $cache->data;
  }
  
  $api_settings = wot_api_settings();
  $query_data = array(
    'application_id' => $api_settings['app_id'],
    'language' => $api_settings['lang'],
    'account_id' => $user_data['account_id'],
    'access_token' => $user_data['access_token']
  );
  
  $full_url = url(wot_api_request_url('account_statistic'), array('query' => $query_data));
  $response = drupal_http_request($full_url);
  if ($api_settings['log_requests_responses']) {
    watchdog(__FUNCTION__, 'Request: @request', array('@request' => $full_url), 
      WATCHDOG_INFO);
    watchdog(__FUNCTION__, 'Responce, the responce: @responce', array('@responce' => var_export($response, true)), 
      WATCHDOG_INFO);
  }
  
  $status = (isset($response->status_message)) ? strtolower($response->status_message) : false;
  if ($status == 'ok') {
    $data = json_decode($response->data, TRUE);
    if ($data['status'] == 'ok') {
      $user_stat = $data['data'][$user_data['account_id']];
      cache_set($cid, $user_stat);
      return $user_stat;
    }
  }
  
  // Report request error
  watchdog('error', 'Bad API server responce, the request: @request', array('@request' => $full_url), 
    WATCHDOG_ERROR);
  
  watchdog('error', 'Bad API server responce, the responce: @responce', array('@responce' => var_export($response, true)), 
    WATCHDOG_ERROR);
  return array();
}

function wot_api_load_user_tank_statistic($tank_id) {
  $user_tanks = wot_api_load_user_tanks();
  foreach ($user_tanks as $tank_stat) {
    if ($tank_stat['tank_id'] == $tank_id) {
      return $tank_stat;
    }
  }

}
