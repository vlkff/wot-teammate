<?php
/**
 * @file
 * wot_platoon_teammate_feature.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wot_platoon_teammate_feature_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'wot_platoon_requests';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'wot_platoon_request';
  $view->human_name = 'WoT Platoon Requests';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Активные заявки других игроков';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Искать взводных';
  $handler->display->display_options['pager']['type'] = 'mini';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['tags']['previous'] = '<< Prev';
  $handler->display->display_options['pager']['options']['tags']['next'] = 'Next >>';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'age' => 'age',
    'gender' => 'gender',
    'voice' => 'voice',
    'tank_tier' => 'tank_tier',
    'tank_nation' => 'tank_nation',
    'tank_model_name' => 'tank_model_name',
    'tank_stat_battles' => 'tank_stat_battles',
    'tank_stat_winrate' => 'tank_stat_winrate',
    'account_stat_battles' => 'account_stat_battles',
    'account_stat_winrate' => 'account_stat_winrate',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'age' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'gender' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'voice' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'tank_tier' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'tank_nation' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'tank_model_name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'tank_stat_battles' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'tank_stat_winrate' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'account_stat_battles' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'account_stat_winrate' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = 'Игроки в этом списке ищут взводного, так же как и вы. Просто скопируйте имя игрока и пригласите его во взвод в игре.';
  $handler->display->display_options['header']['area']['format'] = 'plain_text';
  /* Field: WoT Platoon Request: Wot_account_nickname */
  $handler->display->display_options['fields']['wot_account_nickname']['id'] = 'wot_account_nickname';
  $handler->display->display_options['fields']['wot_account_nickname']['table'] = 'wot_platoon_request';
  $handler->display->display_options['fields']['wot_account_nickname']['field'] = 'wot_account_nickname';
  $handler->display->display_options['fields']['wot_account_nickname']['label'] = 'Имя в игре';
  /* Field: WoT Platoon Request: Age */
  $handler->display->display_options['fields']['age']['id'] = 'age';
  $handler->display->display_options['fields']['age']['table'] = 'wot_platoon_request';
  $handler->display->display_options['fields']['age']['field'] = 'age';
  $handler->display->display_options['fields']['age']['label'] = 'Возраст';
  $handler->display->display_options['fields']['age']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['age']['empty_zero'] = TRUE;
  /* Field: WoT Platoon Request: Gender */
  $handler->display->display_options['fields']['gender']['id'] = 'gender';
  $handler->display->display_options['fields']['gender']['table'] = 'wot_platoon_request';
  $handler->display->display_options['fields']['gender']['field'] = 'gender';
  $handler->display->display_options['fields']['gender']['label'] = 'Пол';
  $handler->display->display_options['fields']['gender']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['gender']['empty_zero'] = TRUE;
  /* Field: WoT Platoon Request: Voice */
  $handler->display->display_options['fields']['voice']['id'] = 'voice';
  $handler->display->display_options['fields']['voice']['table'] = 'wot_platoon_request';
  $handler->display->display_options['fields']['voice']['field'] = 'voice';
  $handler->display->display_options['fields']['voice']['label'] = 'Голосовая связь';
  $handler->display->display_options['fields']['voice']['empty'] = 'Нет';
  $handler->display->display_options['fields']['voice']['empty_zero'] = TRUE;
  /* Field: WoT Platoon Request: Tank_tier */
  $handler->display->display_options['fields']['tank_tier']['id'] = 'tank_tier';
  $handler->display->display_options['fields']['tank_tier']['table'] = 'wot_platoon_request';
  $handler->display->display_options['fields']['tank_tier']['field'] = 'tank_tier';
  $handler->display->display_options['fields']['tank_tier']['label'] = 'Уровень';
  $handler->display->display_options['fields']['tank_tier']['separator'] = '';
  /* Field: WoT Platoon Request: Tank_nation */
  $handler->display->display_options['fields']['tank_nation']['id'] = 'tank_nation';
  $handler->display->display_options['fields']['tank_nation']['table'] = 'wot_platoon_request';
  $handler->display->display_options['fields']['tank_nation']['field'] = 'tank_nation';
  $handler->display->display_options['fields']['tank_nation']['label'] = 'Нация';
  /* Field: WoT Platoon Request: Tank_model_name */
  $handler->display->display_options['fields']['tank_model_name']['id'] = 'tank_model_name';
  $handler->display->display_options['fields']['tank_model_name']['table'] = 'wot_platoon_request';
  $handler->display->display_options['fields']['tank_model_name']['field'] = 'tank_model_name';
  $handler->display->display_options['fields']['tank_model_name']['label'] = 'Танк';
  /* Field: WoT Platoon Request: Tank_stat_battles */
  $handler->display->display_options['fields']['tank_stat_battles']['id'] = 'tank_stat_battles';
  $handler->display->display_options['fields']['tank_stat_battles']['table'] = 'wot_platoon_request';
  $handler->display->display_options['fields']['tank_stat_battles']['field'] = 'tank_stat_battles';
  $handler->display->display_options['fields']['tank_stat_battles']['label'] = 'Боёв на технике';
  /* Field: WoT Platoon Request: Tank_stat_winrate */
  $handler->display->display_options['fields']['tank_stat_winrate']['id'] = 'tank_stat_winrate';
  $handler->display->display_options['fields']['tank_stat_winrate']['table'] = 'wot_platoon_request';
  $handler->display->display_options['fields']['tank_stat_winrate']['field'] = 'tank_stat_winrate';
  $handler->display->display_options['fields']['tank_stat_winrate']['label'] = 'Побед на технике';
  $handler->display->display_options['fields']['tank_stat_winrate']['precision'] = '0';
  $handler->display->display_options['fields']['tank_stat_winrate']['suffix'] = '%';
  /* Field: WoT Platoon Request: Account_stat_battles */
  $handler->display->display_options['fields']['account_stat_battles']['id'] = 'account_stat_battles';
  $handler->display->display_options['fields']['account_stat_battles']['table'] = 'wot_platoon_request';
  $handler->display->display_options['fields']['account_stat_battles']['field'] = 'account_stat_battles';
  $handler->display->display_options['fields']['account_stat_battles']['label'] = 'Боёв на аккаунте';
  /* Field: WoT Platoon Request: Account_stat_winrate */
  $handler->display->display_options['fields']['account_stat_winrate']['id'] = 'account_stat_winrate';
  $handler->display->display_options['fields']['account_stat_winrate']['table'] = 'wot_platoon_request';
  $handler->display->display_options['fields']['account_stat_winrate']['field'] = 'account_stat_winrate';
  $handler->display->display_options['fields']['account_stat_winrate']['label'] = 'Побед на аккаунте';
  $handler->display->display_options['fields']['account_stat_winrate']['precision'] = '0';
  $handler->display->display_options['fields']['account_stat_winrate']['suffix'] = '%';
  /* Sort criterion: WoT Platoon Request: Updated */
  $handler->display->display_options['sorts']['updated']['id'] = 'updated';
  $handler->display->display_options['sorts']['updated']['table'] = 'wot_platoon_request';
  $handler->display->display_options['sorts']['updated']['field'] = 'updated';
  /* Sort criterion: Global: Random */
  $handler->display->display_options['sorts']['random']['id'] = 'random';
  $handler->display->display_options['sorts']['random']['table'] = 'views';
  $handler->display->display_options['sorts']['random']['field'] = 'random';
  $handler->display->display_options['sorts']['random']['expose']['label'] = 'Random';
  /* Contextual filter: WoT Platoon Request: Wot_account_id */
  $handler->display->display_options['arguments']['wot_account_id']['id'] = 'wot_account_id';
  $handler->display->display_options['arguments']['wot_account_id']['table'] = 'wot_platoon_request';
  $handler->display->display_options['arguments']['wot_account_id']['field'] = 'wot_account_id';
  $handler->display->display_options['arguments']['wot_account_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['wot_account_id']['default_argument_type'] = 'php';
  $handler->display->display_options['arguments']['wot_account_id']['default_argument_options']['code'] = '$user_data = wot_api_user();
return $user_data[\'account_id\'];';
  $handler->display->display_options['arguments']['wot_account_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['wot_account_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['wot_account_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['wot_account_id']['not'] = TRUE;
  /* Filter criterion: WoT Platoon Request: Active */
  $handler->display->display_options['filters']['active']['id'] = 'active';
  $handler->display->display_options['filters']['active']['table'] = 'wot_platoon_request';
  $handler->display->display_options['filters']['active']['field'] = 'active';
  $handler->display->display_options['filters']['active']['value']['value'] = '1';
  /* Filter criterion: WoT Platoon Request: Tank_tier */
  $handler->display->display_options['filters']['tank_tier']['id'] = 'tank_tier';
  $handler->display->display_options['filters']['tank_tier']['table'] = 'wot_platoon_request';
  $handler->display->display_options['filters']['tank_tier']['field'] = 'tank_tier';
  $handler->display->display_options['filters']['tank_tier']['exposed'] = TRUE;
  $handler->display->display_options['filters']['tank_tier']['expose']['operator_id'] = 'tank_tier_op';
  $handler->display->display_options['filters']['tank_tier']['expose']['label'] = 'Уровень';
  $handler->display->display_options['filters']['tank_tier']['expose']['operator'] = 'tank_tier_op';
  $handler->display->display_options['filters']['tank_tier']['expose']['identifier'] = 'tank_tier';
  $handler->display->display_options['filters']['tank_tier']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
  );
  $handler->display->display_options['filters']['tank_tier']['is_grouped'] = TRUE;
  $handler->display->display_options['filters']['tank_tier']['group_info']['label'] = 'Уровень';
  $handler->display->display_options['filters']['tank_tier']['group_info']['identifier'] = 'tank_tier';
  $handler->display->display_options['filters']['tank_tier']['group_info']['group_items'] = array(
    1 => array(
      'title' => '1',
      'operator' => '=',
      'value' => array(
        'value' => '1',
        'min' => '',
        'max' => '',
      ),
    ),
    2 => array(
      'title' => '2',
      'operator' => '=',
      'value' => array(
        'value' => '2',
        'min' => '',
        'max' => '',
      ),
    ),
    3 => array(
      'title' => '3',
      'operator' => '=',
      'value' => array(
        'value' => '3',
        'min' => '',
        'max' => '',
      ),
    ),
    4 => array(
      'title' => '4',
      'operator' => '=',
      'value' => array(
        'value' => '4',
        'min' => '',
        'max' => '',
      ),
    ),
    5 => array(
      'title' => '5',
      'operator' => '=',
      'value' => array(
        'value' => '5',
        'min' => '',
        'max' => '',
      ),
    ),
    6 => array(
      'title' => '6',
      'operator' => '=',
      'value' => array(
        'value' => '6',
        'min' => '',
        'max' => '',
      ),
    ),
    7 => array(
      'title' => '7',
      'operator' => '=',
      'value' => array(
        'value' => '7',
        'min' => '',
        'max' => '',
      ),
    ),
    8 => array(
      'title' => '8',
      'operator' => '=',
      'value' => array(
        'value' => '8',
        'min' => '',
        'max' => '',
      ),
    ),
    9 => array(
      'title' => '9',
      'operator' => '=',
      'value' => array(
        'value' => '9',
        'min' => '',
        'max' => '',
      ),
    ),
    10 => array(
      'title' => '10',
      'operator' => '=',
      'value' => array(
        'value' => '10',
        'min' => '',
        'max' => '',
      ),
    ),
  );
  /* Filter criterion: WoT Platoon Request: Tank_model_type */
  $handler->display->display_options['filters']['tank_model_type']['id'] = 'tank_model_type';
  $handler->display->display_options['filters']['tank_model_type']['table'] = 'wot_platoon_request';
  $handler->display->display_options['filters']['tank_model_type']['field'] = 'tank_model_type';
  $handler->display->display_options['filters']['tank_model_type']['exposed'] = TRUE;
  $handler->display->display_options['filters']['tank_model_type']['expose']['operator_id'] = 'tank_model_type_op';
  $handler->display->display_options['filters']['tank_model_type']['expose']['label'] = 'Tank_model_type';
  $handler->display->display_options['filters']['tank_model_type']['expose']['operator'] = 'tank_model_type_op';
  $handler->display->display_options['filters']['tank_model_type']['expose']['identifier'] = 'tank_model_type';
  $handler->display->display_options['filters']['tank_model_type']['is_grouped'] = TRUE;
  $handler->display->display_options['filters']['tank_model_type']['group_info']['label'] = 'Тип техники';
  $handler->display->display_options['filters']['tank_model_type']['group_info']['identifier'] = 'tank_model_type';
  $handler->display->display_options['filters']['tank_model_type']['group_info']['group_items'] = array(
    1 => array(
      'title' => 'Light Tank',
      'operator' => '=',
      'value' => 'lightTank',
    ),
    2 => array(
      'title' => 'Medium Tank',
      'operator' => '=',
      'value' => 'mediumTank',
    ),
    3 => array(
      'title' => 'Heavy Tank',
      'operator' => '=',
      'value' => 'heavyTank',
    ),
    4 => array(
      'title' => 'AT-SPG',
      'operator' => '=',
      'value' => 'AT-SPG',
    ),
    5 => array(
      'title' => 'SPG',
      'operator' => '=',
      'value' => 'SPG',
    ),
  );
  /* Filter criterion: WoT Platoon Request: Expire */
  $handler->display->display_options['filters']['expire']['id'] = 'expire';
  $handler->display->display_options['filters']['expire']['table'] = 'wot_platoon_request';
  $handler->display->display_options['filters']['expire']['field'] = 'expire';
  $handler->display->display_options['filters']['expire']['operator'] = '>';
  $handler->display->display_options['filters']['expire']['value']['value'] = '1';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $translatables['wot_platoon_requests'] = array(
    t('Master'),
    t('Активные заявки других игроков'),
    t('more'),
    t('Искать взводных'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('<< Prev'),
    t('Next >>'),
    t('last »'),
    t('Игроки в этом списке ищут взводного, так же как и вы. Просто скопируйте имя игрока и пригласите его во взвод в игре.'),
    t('Имя в игре'),
    t('Возраст'),
    t('.'),
    t(','),
    t('Пол'),
    t('Голосовая связь'),
    t('Нет'),
    t('Уровень'),
    t('Нация'),
    t('Танк'),
    t('Боёв на технике'),
    t('Побед на технике'),
    t('%'),
    t('Боёв на аккаунте'),
    t('Побед на аккаунте'),
    t('Random'),
    t('All'),
    t('Tank_model_type'),
    t('Тип техники'),
    t('Block'),
  );
  $export['wot_platoon_requests'] = $view;

  return $export;
}
