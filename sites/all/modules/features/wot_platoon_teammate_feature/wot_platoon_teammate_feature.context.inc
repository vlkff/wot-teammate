<?php
/**
 * @file
 * wot_platoon_teammate_feature.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function wot_platoon_teammate_feature_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'frontpage';
  $context->description = '';
  $context->tag = 'wot_teammate';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'block-2' => array(
          'module' => 'block',
          'delta' => '2',
          'region' => 'content',
          'weight' => '-10',
        ),
        'wot_api-login' => array(
          'module' => 'wot_api',
          'delta' => 'login',
          'region' => 'content',
          'weight' => '-9',
        ),
        'wot_teammate-platoon_request' => array(
          'module' => 'wot_teammate',
          'delta' => 'platoon_request',
          'region' => 'content',
          'weight' => '-8',
        ),
        'wot_platoon_teammate_feature-wot_platoon_requests' => array(
          'module' => 'wot_platoon_teammate_feature',
          'delta' => 'wot_platoon_requests',
          'region' => 'content',
          'weight' => '-7',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('wot_teammate');
  $export['frontpage'] = $context;

  return $export;
}
