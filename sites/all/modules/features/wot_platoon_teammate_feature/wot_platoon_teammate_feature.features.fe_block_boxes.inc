<?php
/**
 * @file
 * wot_platoon_teammate_feature.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function wot_platoon_teammate_feature_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Site Intro';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'wot_platoon_site_intro';
  $fe_block_boxes->body = '<div class="intro-3-steps">
<p>Как легко и быстро найти хорошего взводного, который тебе подходит, с которым по фану играть и интересно общаться?</p>
<p>3 простых шага:</p>
<ul>
<li>авторизируйся по wargaming ID</li>
<li>заполни короткую форму о себе и на чем будешь катать.</li>
<li>сайт покажет список игроков, напиши одному из них или жди приглашения во взвод.</li>
</ul>
</div>';

  $export['wot_platoon_site_intro'] = $fe_block_boxes;

  return $export;
}
